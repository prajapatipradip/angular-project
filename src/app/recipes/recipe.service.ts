 import {  Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Recipes } from "./recipes.model";
 
@Injectable()
 export class RecipeService {
   recipeChanged = new Subject<Recipes[]>();
    // recipeSelected = new Subject<Recipes>();
   private recipes: Recipes[] = [
        new Recipes('Tasty Schnitzel',
         'With the gravy',
          'https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG',
           [
              new Ingredient("Cheese", 1),
              new Ingredient("Chilly", 1)

           ]),
        new Recipes('Big Burger',
         'Cheesy and Crunchy',
          'https://upload.wikimedia.org/wikipedia/commons/b/be/Burger_King_Angus_Bacon_%26_Cheese_Steak_Burger.jpg',
          [
            new Ingredient("Coca-cola", 1),
            new Ingredient("Bread", 2)

          ]),
     ];
      constructor(private slService: ShoppingListService) {}
      //setRecipes
      setRecipes(recipes: Recipes[]) {
         this.recipes = recipes;
         this.recipeChanged.next(this.recipes.slice());
      }
     getRecipes() {
         return this.recipes.slice();
     }
     // getRecipe By id
      getRecipe(index: number) {
         return this.recipes[index];
      }
     //add Ingredient
     AddIngredientShopping(ingredients: Ingredient[]) {
         this.slService.addIngredient(ingredients)
     }
     // add Recipe
     addRecipe(recipe: Recipes) {
      this.recipes.push(recipe);
      this.recipeChanged.next(this.recipes.slice())
     }
     //update recipe 
     updateRecipe(index: number, newRecipe:Recipes) {
       this.recipes[index] = newRecipe;
       this.recipeChanged.next(this.recipes.slice())
     }
     //delete 
     deleteRecipe(index: number) {
    this.recipes.splice(index);
     this.recipeChanged.next(this.recipes.slice())

     }
 }