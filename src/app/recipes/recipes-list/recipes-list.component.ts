import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscriber, Subscription } from 'rxjs';
import { RecipeService } from '../recipe.service';
import { Recipes } from '../recipes.model';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css'],
})
export class RecipesListComponent implements OnInit, OnDestroy{

  recipes!: Recipes[] ;
   subscription!: Subscription;

  constructor(private recipeService: RecipeService,
             private router: Router,
                private route: ActivatedRoute) {   
   }

  ngOnInit() {
     this.subscription = this.recipeService.recipeChanged
     .subscribe(
       (recipes: Recipes[]) => {
         this.recipes = recipes;
       }
     )
   this.recipes = this.recipeService.getRecipes();   
  }
  // new Recipe button event
  onNewRecipe() {
   this.router.navigate(['new'], { relativeTo: this.route})
  }
  ngOnDestroy() {
       this.subscription.unsubscribe();
  }



}
