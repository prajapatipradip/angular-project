import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { RecipeService } from "../recipes/recipe.service";
import { Recipes } from "../recipes/recipes.model";

@Injectable({
 providedIn:'root'
})
export class DataStorageService {
      constructor(private http: HttpClient,
                 private recipeService:RecipeService) {}
      // store recipe method
      storeRecipe() {
              const recipes = this.recipeService.getRecipes();
              this.http.put('https://angular-d2ad0-default-rtdb.firebaseio.com/recipes.json', recipes)
              .subscribe(response => {
                console.log(response);
  

              }) 
      }
      // fetch recipes
      fetchrecipe() {
          this.http.get<Recipes[]>('https://angular-d2ad0-default-rtdb.firebaseio.com/recipes.json')
          .subscribe( recipes => {
             this.recipeService.setRecipes(recipes);
              
          })
      }
}