import { Component } from "@angular/core";
import { DataStorageService } from "../shared/data-Storage.service";

@Component( {
      selector:"app-header",
      templateUrl:"./header.component.html"
})
export class HeaderComponent {
      constructor(private datastorageservice: DataStorageService){ }
      onSaveData() {
         this.datastorageservice.storeRecipe();
      }
      // fetch data
      onFetchData() {
            this.datastorageservice.fetchrecipe();
      }
     
}