 
import { Subject } from "rxjs";
import { Ingredient } from "../shared/ingredient.model";
 export class ShoppingListService {
     ingredientChanged = new Subject<Ingredient[]>();
      startedEditing = new Subject<number>();
    ingredients: Ingredient[] = [
        new Ingredient('apple', 6),
        new Ingredient('banana', 2),
     ];
     
     // get list
     getIngredients() {
         return this.ingredients.slice();
     }
     // getIngredent to form
      getIngredient(index: number) {
        return this.ingredients[index];
      }
    // addIngredients
     addIngredients(ingredient : Ingredient) {
       this.ingredients.push(ingredient)
        this.ingredientChanged.next(this.ingredients.slice())
     }
     // addIngredi
     addIngredient(ingredients: Ingredient[]) {
        this.ingredients.push(...ingredients)
        this.ingredientChanged.next(this.ingredients.slice())
     }
     // Update Ingredient
      updateIngredient(index: number, newIngredient: Ingredient) {
           this.ingredients[index] = newIngredient;
            this.ingredientChanged.next(this.ingredients.slice())
      }
      //deleteIngredient
       deleteIngredient(index: number) {
        this.ingredients.splice(index, 1);
        this.ingredientChanged.next(this.ingredients.slice())
       }

 }